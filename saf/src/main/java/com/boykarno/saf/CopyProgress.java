package com.boykarno.saf;

public interface CopyProgress {
    void OnCopyProgress(boolean isError, double progress);
}
