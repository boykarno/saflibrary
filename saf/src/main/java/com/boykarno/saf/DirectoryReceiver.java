package com.boykarno.saf;

public interface DirectoryReceiver
{
    void OnDirectoryPicked( String rawUri, String path, String name );
}
