package com.boykarno.saf;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.util.Log;

import androidx.documentfile.provider.DocumentFile;

import com.anggrayudi.storage.file.*;

import java.util.ArrayList;

@TargetApi( Build.VERSION_CODES.Q )
public class DirectoryPickerFragment extends Fragment
{
    private static final int DIRECTORY_PICK_REQUEST_CODE = 74425;
    private static final int FILE_PICK_REQUEST_CODE = 74426;

    private final DirectoryReceiver directoryReceiver;
    private String initialPath = "";
    //private boolean isPickingFile = false;
    private int mode; //0=file temporary access, 1=file, 2=directory
    private String[] filter;

    public DirectoryPickerFragment()
    {
        directoryReceiver = null;
    }

    @SuppressLint("ValidFragment")
    public DirectoryPickerFragment(final DirectoryReceiver directoryReceiver )
    {
        this.directoryReceiver = directoryReceiver;
    }

    @SuppressLint("ValidFragment")
    public DirectoryPickerFragment(final DirectoryReceiver directoryReceiver, String initialPath, int mode, String[] filter )
    {
        this.directoryReceiver = directoryReceiver;
        this.initialPath = initialPath;
        this.mode = mode;
        this.filter = filter;
    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        if( directoryReceiver == null )
            getFragmentManager().beginTransaction().remove( this ).commit();
        else
        {
            String action = Intent.ACTION_GET_CONTENT;
            if(mode == 1)
                action = Intent.ACTION_OPEN_DOCUMENT;
            else if(mode == 2)
                action = Intent.ACTION_OPEN_DOCUMENT_TREE;

            Intent intent = new Intent(action);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);

            // Try to set the initial folder of the picker as sdcard root
            intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
            intent.putExtra("android.content.extra.FANCY", true);
            intent.putExtra("android.content.extra.SHOW_FILESIZE", true);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);

            if (initialPath == "") {
                initialPath = "content://com.android.externalstorage.documents/tree/primary%3A/document/primary%3A";
            }

            Uri uri = Uri.parse(initialPath);
            intent.putExtra("android.provider.extra.INITIAL_URI", uri);

            if(mode == 0 || mode == 1) {
                intent.setType("*/*");

                if (filter != null && filter.length > 0) {
                    String[] mimetypes = filter;
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
                }
            }

            startActivityForResult(intent, mode == 0 || mode == 1 ? FILE_PICK_REQUEST_CODE : DIRECTORY_PICK_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        if( !(requestCode == DIRECTORY_PICK_REQUEST_CODE || requestCode == FILE_PICK_REQUEST_CODE))
            return;

        String rawUri = "";
        String name = "";
        String path = "";
        if( resultCode == Activity.RESULT_OK && data != null )
        {
            Uri entryUri = data.getData();
            if( entryUri != null )
            {
                SAFEntry entry = mode == 0 || mode == 1 ? new SAFEntry(getContext(), entryUri) : SAFEntry.fromTreeUri( getActivity(), entryUri );
                if( entry != null && entry.exists() )
                {
                    Activity activity = getActivity();

                    rawUri = entry.getUri().toString();
                    name = entry.getName();
                    path = entry.getAbsolutePath();

                    Log.d("SAF", "mode: " + mode + " ,rawUri: " + rawUri + " ,path: " + path);

                    if(mode > 0)
                        activity.getContentResolver().takePersistableUriPermission( data.getData(), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION );
                }
            }
        }

        if( directoryReceiver != null )
            directoryReceiver.OnDirectoryPicked( rawUri, path, name );

        getFragmentManager().beginTransaction().remove( this ).commit();
    }
}