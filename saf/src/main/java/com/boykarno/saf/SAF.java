package com.boykarno.saf;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriPermission;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Debug;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.annotation.RequiresApi;
import androidx.documentfile.provider.DocumentFile;

import com.anggrayudi.storage.SimpleStorage;
import com.anggrayudi.storage.SimpleStorageHelper;
import com.anggrayudi.storage.file.DocumentFileCompat;
import com.anggrayudi.storage.file.DocumentFileUtils;
import com.anggrayudi.storage.media.FileDescription;
import com.anggrayudi.storage.media.MediaFile;
import com.anggrayudi.storage.media.MediaStoreCompat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class SAF {

    private static final int maxCallbackMessage = 30;

    @TargetApi( Build.VERSION_CODES.Q )
    private static class UriPermissionSorter implements Comparator<UriPermission>
    {
        public int compare( UriPermission a, UriPermission b )
        {
            long difference = b.getPersistedTime() - a.getPersistedTime();
            if( difference < 0 )
                return -1;
            if( difference > 0 )
                return 1;

            return 0;
        }
    }

    private static final StringBuilder stringBuilder = new StringBuilder();

    public static String GetExternalDrives()
    {
        File primary = Environment.getExternalStorageDirectory();
        String primaryPath = primary.getAbsolutePath();

        stringBuilder.setLength( 0 );
        stringBuilder.append( primaryPath ).append( ":" );

        // Try paths saved at system environments
        // Credit: https://stackoverflow.com/a/32088396/2373034
        String strSDCardPath = System.getenv( "SECONDARY_STORAGE" );
        if( strSDCardPath == null || strSDCardPath.length() == 0 )
            strSDCardPath = System.getenv( "EXTERNAL_SDCARD_STORAGE" );

        if( strSDCardPath != null && strSDCardPath.length() > 0 )
        {
            String[] externalPaths = strSDCardPath.split( ":" );
            for( int i = 0; i < externalPaths.length; i++ )
            {
                String path = externalPaths[i];
                if( path != null && path.length() > 0 )
                {
                    File file = new File( path );
                    if( file.exists() && file.isDirectory() && file.canRead() && !file.getAbsolutePath().equalsIgnoreCase( primaryPath ) )
                    {
                        String absolutePath = file.getAbsolutePath() + File.separator + "Android";
                        if( new File( absolutePath ).exists() )
                        {
                            try
                            {
                                // Check if two paths lead to same storage (aliases)
                                if( !primary.getCanonicalPath().equals( file.getCanonicalPath() ) )
                                    stringBuilder.append( file.getAbsolutePath() ).append( ":" );
                            }
                            catch( Exception e )
                            {
                            }
                        }
                    }
                }
            }
        }

        // Try most common possible paths
        // Credit: https://gist.github.com/PauloLuan/4bcecc086095bce28e22
        String[] possibleRoots = new String[] { "/storage", "/mnt", "/storage/removable",
                "/removable", "/data", "/mnt/media_rw", "/mnt/sdcard0" };
        for( String root : possibleRoots )
        {
            try
            {
                File[] fileList = new File( root ).listFiles();
                for( File file : fileList )
                {
                    if( file.exists() && file.isDirectory() && file.canRead() && !file.getAbsolutePath().equalsIgnoreCase( primaryPath ) )
                    {
                        String absolutePath = file.getAbsolutePath() + File.separator + "Android";
                        if( new File( absolutePath ).exists() )
                        {
                            try
                            {
                                // Check if two paths lead to same storage (aliases)
                                if( !primary.getCanonicalPath().equals( file.getCanonicalPath() ) )
                                    stringBuilder.append( file.getAbsolutePath() ).append( ":" );
                            }
                            catch( Exception ex )
                            {
                            }
                        }
                    }
                }
            }
            catch( Exception e )
            {
            }
        }

        return stringBuilder.toString();
    }


    @TargetApi( Build.VERSION_CODES.M )
    public static int CheckPermission( Context context )
    {
        if( Build.VERSION.SDK_INT < Build.VERSION_CODES.M )
            return 1;

        if( context.checkSelfPermission( Manifest.permission.WRITE_EXTERNAL_STORAGE ) == PackageManager.PERMISSION_GRANTED &&
                context.checkSelfPermission( Manifest.permission.READ_EXTERNAL_STORAGE ) == PackageManager.PERMISSION_GRANTED )
            return 1;

        return 0;
    }

    // Credit: https://github.com/Over17/UnityAndroidPermissions/blob/0dca33e40628f1f279decb67d901fd444b409cd7/src/UnityAndroidPermissions/src/main/java/com/unity3d/plugin/UnityAndroidPermissions.java
    public static void RequestPermission( Context context, final PermissionReceiver permissionReceiver, final int lastCheckResult )
    {
        if( CheckPermission( context ) == 1 )
        {
            permissionReceiver.OnPermissionResult( 1 );
            return;
        }

        if( lastCheckResult == 0 ) // If user clicked "Don't ask again" before, don't bother asking them again
        {
            permissionReceiver.OnPermissionResult( 0 );
            return;
        }

        final Fragment request = new PermissionFragment( permissionReceiver );
        ( (Activity) context ).getFragmentManager().beginTransaction().add( 0, request ).commit();
    }

    // Returns whether or not Storage Access Framework (SAF) should be used
    public static boolean CheckSAF()
    {
        return android.os.Build.VERSION.SDK_INT >= 29 && !Environment.isExternalStorageLegacy();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public static boolean HavePermissionOnPath(Context context, String path){
        List<UriPermission> uriPermissions = context.getContentResolver().getPersistedUriPermissions();
        uriPermissions.sort( new UriPermissionSorter() );

        //Log.d("SAF","===== CanAccessDirectoryOrFile =====");

        //boolean result = false;
        for( int i = 0; i < uriPermissions.size(); i++ )
        {
            UriPermission uriPermission = uriPermissions.get( i );
            SAFEntry directory = SAFEntry.fromTreeUri( context, uriPermission.getUri() );
            if( directory != null && directory.exists() && directory.isDirectory() )
            {
                String currentPath = directory.getAbsolutePath();
                //Log.d("SAF", "currentPath: " +currentPath + " ,path: " + path);
                if(currentPath.equals(path)){
                    return true;
                }
            }
        }

        return false;
    }


    // Prompts the user to pick a Storage Access Framework (SAF) folder
    @TargetApi( Build.VERSION_CODES.Q )
    public static void PickSAFFolder( Context context, final DirectoryReceiver directoryReceiver )
    {
        PickSAFEntry(context, directoryReceiver, "", 2, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void PickSAFFolder(Context context, final DirectoryReceiver directoryReceiver, String rootDirectoryName )
    {
        String initialPath = "";
        if(rootDirectoryName == Environment.DIRECTORY_DOWNLOADS){
            initialPath = "content://com.android.providers.downloads.documents/tree/downloads/document/downloads";
        }
        else if(rootDirectoryName == "Root"){
            initialPath = "";
        }
        else {
            initialPath = "content://com.android.externalstorage.documents/tree/primary%3A" + rootDirectoryName + "/document/primary%3A" + rootDirectoryName;
        }

        PickSAFEntry(context, directoryReceiver, initialPath, 2, null);
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static void PickSAFEntry( Context context, final DirectoryReceiver directoryReceiver, String initialPath , int mode, Object filtersObj)
    {
        String[] filters = filtersObj == null? null : (String[])filtersObj;

        final Fragment request = new DirectoryPickerFragment( directoryReceiver, initialPath ,mode ,filters);
        ( (Activity) context ).getFragmentManager().beginTransaction().add( 0, request ).commit();
    }

    /*
    @RequiresApi(api = Build.VERSION_CODES.Q)
    public static String ReadTextFromLinks(Context context, String simplePath ){
        ArrayList<SAFEntry> entries = GetSAFQuickLinks(context);
        String[] parentDir = GetParentDirFromPath(simplePath);

        for(int i=0;i < parentDir.length ;i++){
            SAFEntry entry = GetEntryFromLinks(entries, parentDir[i]);
            if(entry != null){
                entries = entry.listFiles();
            }else {
                break;
            }

            //found!
            if(i == parentDir.length - 1){
                String path = entry.getUri().toString();
                Log.d("SAF", "Reading from: " + path);
                return ReadTextFromPath(context, path);
            }
        }

        return "";
    }

    private static SAFEntry GetEntryFromLinks(ArrayList<SAFEntry> entries, String name){
        for(int i=0;i<entries.size();i++){
            SAFEntry entry  = entries.get(i);
            if(entry.getName().equals(name)){
                return entry;
            }
        }

        return null;
    }

    //Documents/BUSSID/Musics/ => Documents,Bussid,Musics
    private static String[] GetParentDirFromPath(String simplePath){
        ArrayList<String> result = new ArrayList<String>();
        String[] split = simplePath.split("/");
        for(int i=0;i<split.length;i++){
            if(split[i]!=""){
                result.add(split[i]);
            }
        }

        return  result.toArray(new String[0]);
    }
     */

    // Retrieves the previously picked Storage Access Framework (SAF) folder uris
    @TargetApi( Build.VERSION_CODES.Q )
    public static String FetchSAFQuickLinks( Context context)
    {
        ArrayList<SAFEntry> entries = GetSAFQuickLinks(context);
        String result = "";
        for(int i=0;i<entries.size();i++){
            result += entries.get(i).ToRawData() + "\n";
        }

        Log.d("SAF","FetchSAFQuickLinks: " + result);
        return result;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private static ArrayList<SAFEntry> GetSAFQuickLinks(Context context){
        List<UriPermission> uriPermissions = context.getContentResolver().getPersistedUriPermissions();
        uriPermissions.sort( new UriPermissionSorter() );

        ArrayList<SAFEntry> result = new ArrayList<SAFEntry>();

        for( int i = 0; i < uriPermissions.size(); i++ )
        {
            UriPermission uriPermission = uriPermissions.get( i );

            if(uriPermission.getPersistedTime() == UriPermission.INVALID_TIME || !uriPermission.isReadPermission() || !uriPermission.isWritePermission() )
                context.getContentResolver().releasePersistableUriPermission( uriPermission.getUri(), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION );
            else
            {
                SAFEntry directory = SAFEntry.fromTreeUri( context, uriPermission.getUri() );
                if( directory != null && directory.exists() && directory.isDirectory() )
                {
                    //result += directory.ToRawData() + "\n";
                    result.add(directory);
                }
                else
                    context.getContentResolver().releasePersistableUriPermission( uriPermission.getUri(), Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION );
            }
        }

        return result;
    }


    // Copies/moves a Storage Access Framework (SAF) file/raw file
    @TargetApi( Build.VERSION_CODES.Q )
    public static void CopyFile( Context context, String sourceRawUri, String destinationRawUri, boolean isMoveOperation )
    {
        CopyFile(context, sourceRawUri, destinationRawUri, isMoveOperation, null);
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static void CopyFile( Context context, String sourceRawUri, String destinationRawUri, boolean isMoveOperation , CopyProgress progress){
        // Parameters can point to either SAF files or raw filesystem files
        boolean isSourceSAFFile = sourceRawUri.contains( "://" );
        boolean isDestinationSAFFile = destinationRawUri.contains( "://" );

        if( isSourceSAFFile )
        {
            if( isDestinationSAFFile )
            {
                // Copy SAF file to SAF file
                CopySAFEntry( context, sourceRawUri, destinationRawUri , progress);
            }
            else
            {
                // Copy SAF file to raw file
                ReadFromSAFEntry( context, sourceRawUri, destinationRawUri );
            }
        }
        else
        {
            if( isDestinationSAFFile )
            {
                // Copy raw file to SAF file
                WriteToSAFEntry( context, destinationRawUri, sourceRawUri, false );
            }
            else
            {
                // Copy raw file to raw file
                CopyRawFile( sourceRawUri, destinationRawUri );
            }
        }

        if( isMoveOperation )
        {
            if( isSourceSAFFile )
                DeleteSAFEntry( context, sourceRawUri );
            else
                new File( sourceRawUri ).delete();
        }
    }

    // Copies/moves a Storage Access Framework (SAF) directory/raw directory
    @TargetApi( Build.VERSION_CODES.Q )
    public static void CopyDirectory( Context context, String sourceRawUri, String destinationRawUri, boolean isMoveOperation )
    {
        // Parameters can point to either SAF directories or raw filesystem directories
        boolean isSourceSAFDirectory = sourceRawUri.contains( "://" );
        boolean isDestinationSAFDirectory = destinationRawUri.contains( "://" );

        if( isSourceSAFDirectory )
            CopySAFDirectoryRecursively( context, new SAFEntry( context, Uri.parse( sourceRawUri ) ), destinationRawUri, isDestinationSAFDirectory );
        else
            CopyRawDirectoryRecursively( context, new File( sourceRawUri ), destinationRawUri, isDestinationSAFDirectory );

        if( isMoveOperation )
        {
            if( isSourceSAFDirectory )
                DeleteSAFEntry( context, sourceRawUri );
            else
                DeleteRawDirectoryRecursively( new File( sourceRawUri ) );
        }
    }

    // Fetches the contents of a Storage Access Framework (SAF) folder
    @TargetApi( Build.VERSION_CODES.Q )
    public static String OpenSAFFolder( Context context, String rawUri )
    {
        SAFEntry directory = new SAFEntry( context, Uri.parse( rawUri ) );
        ArrayList<SAFEntry> entries = directory.listFiles();

        String result = "";
        int cnt = entries.size();
        for( int i = 0; i < cnt; i++ )
        {
            SAFEntry entry = entries.get( i );
            result += entry.ToRawData();
            if(i<cnt - 1)
                result += "\n";
        }

        return  result;
    }


    public static String ListDirectoryContent(Context context, String sourceAbsolutePath){
        DocumentFile folder = DocumentFile.fromFile(new File(sourceAbsolutePath));
        DocumentFile[] contents = folder.listFiles();
        String result = "";

        for(int i=0;i<contents.length;i++){
            String path = contents[i].getUri().getPath();
            result += contents[i].getName() + "#" + path + "#" + path + "#" + (contents[i].isDirectory()? "d" : "f");

            if(i<contents.length - 1)
                result += "\n";
        }

        return result;
    }

    // Creates a new Storage Access Framework (SAF) file/folder
    @TargetApi( Build.VERSION_CODES.Q )
    public static String CreateSAFEntry( Context context, String folderRawUri, boolean isFolder, String name )
    {
        SAFEntry directory = new SAFEntry( context, Uri.parse( folderRawUri ) );
        if( isFolder )
            return directory.createDirectory( name ).getUri().toString();

        String mimeType = GetExtensionFromName(name);

        return directory.createFile( mimeType, name ).getUri().toString();
    }

    private static String GetExtensionFromName(String fileName){
        int extensionSeparator = fileName.lastIndexOf( '.' );
        String extension = extensionSeparator >= 0 ? fileName.substring( extensionSeparator + 1 ) : "";

        // Credit: https://stackoverflow.com/a/31691791/2373034
        String mimeType = extension.length() > 0 ? MimeTypeMap.getSingleton().getMimeTypeFromExtension( extension.toLowerCase( Locale.ENGLISH ) ) : null;
        if( mimeType == null || mimeType.length() == 0 )
            mimeType = "application/octet-stream";

        return mimeType;
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static void WriteToSAFEntry( Context context, String rawUri, String sourceFile, boolean appendMode )
    {
        try
        {
            InputStream input = new FileInputStream( new File( sourceFile ) );
            try
            {
                Uri targetUri = Uri.parse( rawUri );
                SAFEntry targetFile = new SAFEntry(context, targetUri);
                if(!targetFile.exists()){
                    String folder = rawUri.substring(0, rawUri.lastIndexOf("%2F"));
                    String fileName = rawUri.substring(rawUri.lastIndexOf("%2F") + 3);
                    Log.d("SAF","folder: " + folder + " ,filename: " + fileName);
                    CreateSAFEntry(context, folder, false, fileName);
                }

                OutputStream output = context.getContentResolver().openOutputStream( targetUri, appendMode ? "wa" : "rwt" );
                if( output == null )
                    return;

                try
                {
                    byte[] buf = new byte[4096];
                    int len;
                    while( ( len = input.read( buf ) ) > 0 )
                        output.write( buf, 0, len );
                }
                finally
                {
                    output.close();
                }
            }
            finally
            {
                input.close();
            }
        }
        catch( Exception e )
        {
            Log.e( "Unity", "Exception:", e );
        }
    }

    public static String ReadTextFromPath(Context context, String treeUri) {
        DocumentFile file = DocumentFile.fromTreeUri(context, Uri.parse(treeUri));
        if(file == null){
            Log.e("SAF", "FILE IS NULL!!");
            return "";
        }

        String result = "";
        try {
            InputStream inputStream = DocumentFileUtils.openInputStream(file, context);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                result = stringBuilder.toString();
            }else {
                Log.e("SAF", "InputStream IS NULL!!");
            }
        }
        catch (FileNotFoundException e) {
            Log.e("SAF", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("SAF", "Can not read file: " + e.toString());
        }

        return result;
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static void ReadFromSAFEntry( Context context, String rawUri, String destinationFile )
    {
        try
        {
            InputStream input = context.getContentResolver().openInputStream( Uri.parse( rawUri ) );
            if( input == null )
                return;

            try
            {
                //String ext = GetExtensionFromPath(destinationFile);
                //String tempPath = destinationFile.replace(ext, "bak");

                File result = new File( destinationFile);
                OutputStream output = new FileOutputStream( result, false );
                try
                {
                    byte[] buf = new byte[4096];
                    int len;
                    while( ( len = input.read( buf ) ) > 0 )
                        output.write( buf, 0, len );
                }
                finally
                {
                    output.close();
                }

                //.renameTo(new File(destinationFile));
            }
            finally
            {
                input.close();
            }
        }
        catch( Exception e )
        {
            Log.e( "Unity", "Exception:", e );
        }
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static void CopySAFEntry( Context context, String sourceRawUri, String destinationRawUri )
    {
        CopySAFEntry(context, sourceRawUri, destinationRawUri, null);
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static void CopySAFEntry( Context context, String sourceRawUri, String destinationRawUri , CopyProgress progress){
        try
        {
            InputStream input = context.getContentResolver().openInputStream( Uri.parse( sourceRawUri ) );
            if( input == null )
                return;

            try
            {
                destinationRawUri = destinationRawUri.replaceAll("%20"," ");
                Uri targetUri = Uri.parse( destinationRawUri );
                SAFEntry targetFile = new SAFEntry(context, targetUri);
                if(!targetFile.exists()){
                    String folder = destinationRawUri.substring(0, destinationRawUri.lastIndexOf("%2F"));
                    String fileName = destinationRawUri.substring(destinationRawUri.lastIndexOf("%2F") + 3);
                    CreateSAFEntry(context, folder, false, fileName);
                }

                OutputStream output = context.getContentResolver().openOutputStream( Uri.parse( destinationRawUri ), "rwt" );
                if( output == null )
                    return;

                try
                {
                    byte[] buf = new byte[4096];
                    int len;

                    SAFEntry sourceFile = new SAFEntry(context, Uri.parse( sourceRawUri ));
                    long length = sourceFile.length();
                    double callbackChunkSize = (double)length / maxCallbackMessage;
                    double nextCallback = callbackChunkSize;
                    double total = 0;

                    while( ( len = input.read( buf ) ) > 0 )
                    {
                        total += len;
                        if(progress != null && total >= nextCallback){
                            nextCallback += callbackChunkSize;
                            progress.OnCopyProgress(false,total / length);
                        }

                        output.write(buf, 0, len);
                    }
                }
                finally
                {
                    output.close();
                    if(progress != null){
                        progress.OnCopyProgress(false,1);
                    }
                }
            }
            finally
            {
                input.close();
            }
        }
        catch( Exception e )
        {
            Log.e( "Unity", "Exception:", e );
            if(progress != null){
                progress.OnCopyProgress(true,1);
            }
        }
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static boolean SAFEntryExists( Context context, String rawUri, boolean isDirectory )
    {
        boolean isSAF = rawUri.contains( "://" );
        if(isSAF) {
            SAFEntry entry = new SAFEntry(context, Uri.parse(rawUri));
            return entry.exists() && entry.isDirectory() == isDirectory;
        }else{
            File file = new File(rawUri);
            return file.exists() && file.isDirectory() == isDirectory;
        }
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static boolean SAFEntryDirectory( Context context, String rawUri )
    {
        return new SAFEntry( context, Uri.parse( rawUri ) ).isDirectory();
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static String SAFEntryName( Context context, String rawUri )
    {
        return new SAFEntry( context, Uri.parse( rawUri ) ).getName();
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static long SAFEntrySize( Context context, String rawUri )
    {
        return new SAFEntry( context, Uri.parse( rawUri ) ).length();
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static long SAFEntryLastModified( Context context, String rawUri )
    {
        return new SAFEntry( context, Uri.parse( rawUri ) ).lastModified();
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static String RenameSAFEntry( Context context, String rawUri, String newName )
    {
        return new SAFEntry( context, Uri.parse( rawUri ) ).renameTo( newName );
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static boolean DeleteSAFEntry( Context context, String rawUri )
    {
        return new SAFEntry( context, Uri.parse( rawUri ) ).delete();
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static String GetParentDirectory( Context context, String rawUri )
    {
        try
        {
            if( !rawUri.contains( "://" ) )
            {
                // This is a raw filepath, not a SAF path
                //String parentPath = new File( rawUri ).getParent();
                DocumentFile file = DocumentFile.fromFile(new File(rawUri)).getParentFile();
                return file.getUri().getPath();
                //return parentPath != null ? parentPath : "";
            }

            // The most promising method is to calculate the URI's path using findDocumentPath, omit the last path segment from it
            // and then replace the rawUri's path entirely
            DocumentsContract.Path rawUriPath = DocumentsContract.findDocumentPath( context.getContentResolver(), Uri.parse( rawUri ) );
            if( rawUriPath != null )
            {
                List<String> pathSegments = rawUriPath.getPath();
                if( pathSegments != null && pathSegments.size() > 0 )
                {
                    String rawUriParentPath;
                    if( pathSegments.size() > 1 )
                        rawUriParentPath = Uri.encode( pathSegments.get( pathSegments.size() - 2 ) );
                    else
                    {
                        String fullPath = pathSegments.get( 0 );
                        int separatorIndex = Math.max( fullPath.lastIndexOf( '/' ), fullPath.lastIndexOf( ':' ) + 1 );
                        rawUriParentPath = separatorIndex > 0 ? Uri.encode( fullPath.substring( 0, separatorIndex ) ) : null;
                    }

                    if( rawUriParentPath != null && rawUriParentPath.length() > 0 )
                    {
                        int rawUriLastPathSegmentIndex = rawUri.lastIndexOf( '/' ) + 1;
                        if( rawUriLastPathSegmentIndex > 0 )
                        {
                            String parentRawUri = rawUri.substring( 0, rawUriLastPathSegmentIndex ) + rawUriParentPath;
                            if( !parentRawUri.equals( rawUri ) && SAFEntryExists( context, parentRawUri, true ) )
                                return parentRawUri;
                        }
                    }
                }
            }

            // Omit the last path segment (this method won't work for Downloads folder and probably some other ContentProviders, too)
            int pathSeparatorIndex = rawUri.lastIndexOf( "%3A" ); // Encoded colon index
            if( pathSeparatorIndex > 0 )
                pathSeparatorIndex += 3; // Encoded colon shouldn't be omitted by substring

            pathSeparatorIndex = Math.max( pathSeparatorIndex, Math.max( rawUri.lastIndexOf( '/' ), rawUri.lastIndexOf( "%2F" ) ) );
            if( pathSeparatorIndex < 0 || pathSeparatorIndex >= rawUri.length() )
                return "";

            rawUri = rawUri.substring( 0, pathSeparatorIndex );

            if( SAFEntryExists( context, rawUri, true ) )
                return rawUri;

            // When we form the SAF URI using a subfolder as root (i.e. /storage/SomeFolder/), that subfolder is reflected in SAF URI
            // in the form /tree/primary%3ASomeFolder/ and restricts our access to SomeFolder's parent directories. However, if we
            // actually have permission to access the /storage/ directory (parent folder), we can remove the subfolder from the URI
            // (i.e. change it to /tree/primary%3A/) and voila!
            int treeStartIndex = rawUri.indexOf( "/tree/" );
            if( treeStartIndex >= 0 )
            {
                treeStartIndex += 6;
                int treeEndIndex = rawUri.indexOf( '/', treeStartIndex );
                if( treeEndIndex > treeStartIndex + 4 ) // +4: "/tree/SOMETHING/" here, SOMETHING should be able to contain at least 1 %2F/%3A and 1 other character
                {
                    String treeComponent = rawUri.substring( treeStartIndex, treeEndIndex );
                    String preTreeComponent = rawUri.substring( 0, treeStartIndex );
                    String postTreeComponent = rawUri.substring( treeEndIndex );

                    String _treeComponent = treeComponent;
                    int treeSeparatorIndex = _treeComponent.length() - 3; // -3: if treeComponent ends with %2F, skip it
                    while( ( treeSeparatorIndex = _treeComponent.lastIndexOf( "%2F", treeSeparatorIndex - 1 ) ) > 0 )
                    {
                        _treeComponent = _treeComponent.substring( 0, treeSeparatorIndex );

                        String _rawUri = preTreeComponent + _treeComponent + postTreeComponent;
                        if( SAFEntryExists( context, _rawUri, true ) )
                            return _rawUri;
                    }

                    _treeComponent = treeComponent;
                    treeSeparatorIndex = _treeComponent.length() - 3; // -3: if treeComponent ends with %3A, skip it
                    while( ( treeSeparatorIndex = _treeComponent.lastIndexOf( "%3A", treeSeparatorIndex - 1 ) ) > 0 )
                    {
                        _treeComponent = _treeComponent.substring( 0, treeSeparatorIndex + 3 ); // Encoded colon (%3A) shouldn't be omitted by substring

                        String _rawUri = preTreeComponent + _treeComponent + postTreeComponent;
                        if( SAFEntryExists( context, _rawUri, true ) )
                            return _rawUri;
                    }
                }
            }
        }
        catch( Exception e )
        {
            Log.e( "Unity", "Exception:", e );
        }

        return "";
    }

    @TargetApi( Build.VERSION_CODES.Q )
    public static void CopyRawDocumentFile(Context context, String sourcePath, String destinationPath ){
        try
        {
            DocumentFile file = DocumentFile.fromFile(new File( sourcePath ));
            InputStream input = DocumentFileUtils.openInputStream(file, context);
            try
            {
                OutputStream output = new FileOutputStream( new File( destinationPath ), false );
                try
                {
                    byte[] buf = new byte[4096];
                    int len;
                    while( ( len = input.read( buf ) ) > 0 )
                        output.write( buf, 0, len );
                }
                finally
                {
                    output.close();
                }
            }
            finally
            {
                input.close();
            }
        }
        catch( Exception e )
        {
            Log.e( "Unity", "Exception:", e );
        }
    }


    @TargetApi( Build.VERSION_CODES.Q )
    private static void CopyRawFile( String sourcePath, String destinationPath )
    {
        try
        {
            InputStream input = new FileInputStream( new File( sourcePath ) );
            try
            {
                OutputStream output = new FileOutputStream( new File( destinationPath ), false );
                try
                {
                    byte[] buf = new byte[4096];
                    int len;
                    while( ( len = input.read( buf ) ) > 0 )
                        output.write( buf, 0, len );
                }
                finally
                {
                    output.close();
                }
            }
            finally
            {
                input.close();
            }
        }
        catch( Exception e )
        {
            Log.e( "Unity", "Exception:", e );
        }
    }

    @TargetApi( Build.VERSION_CODES.Q )
    private static void CopySAFDirectoryRecursively( Context context, SAFEntry sourceDirectory, String destinationRawUri, boolean isDestinationSAFDirectory )
    {
        File destinationDirectory = null;
        ArrayList<SAFEntry> destinationContents = null;
        if( isDestinationSAFDirectory )
            destinationContents = new SAFEntry( context, Uri.parse( destinationRawUri ) ).listFiles();
        else
        {
            destinationDirectory = new File( destinationRawUri );
            destinationDirectory.mkdirs();
        }

        ArrayList<SAFEntry> contents = sourceDirectory.listFiles();
        for( int i = 0; i < contents.size(); i++ )
        {
            SAFEntry content = contents.get( i );
            if( content.isDirectory() )
            {
                String targetRawUri;
                if( isDestinationSAFDirectory )
                    targetRawUri = FindSAFEntryWithNameOrCreateNew( context, destinationRawUri, destinationContents, true, content.getName() );
                else
                    targetRawUri = new File( destinationDirectory, content.getName() ).getPath();

                CopySAFDirectoryRecursively( context, content, targetRawUri, isDestinationSAFDirectory );
            }
            else
            {
                if( isDestinationSAFDirectory )
                {
                    String targetRawUri = FindSAFEntryWithNameOrCreateNew( context, destinationRawUri, destinationContents, false, content.getName() );
                    CopySAFEntry( context, content.getUri().toString(), targetRawUri );
                }
                else
                {
                    String targetRawUri = new File( destinationDirectory, content.getName() ).getPath();
                    ReadFromSAFEntry( context, content.getUri().toString(), targetRawUri );
                }
            }
        }
    }

    @TargetApi( Build.VERSION_CODES.Q )
    private static void CopyRawDirectoryRecursively( Context context, File sourceDirectory, String destinationRawUri, boolean isDestinationSAFDirectory )
    {
        File destinationDirectory = null;
        ArrayList<SAFEntry> destinationContents = null;
        if( isDestinationSAFDirectory )
            destinationContents = new SAFEntry( context, Uri.parse( destinationRawUri ) ).listFiles();
        else
        {
            destinationDirectory = new File( destinationRawUri );
            destinationDirectory.mkdirs();
        }

        File[] contents = sourceDirectory.listFiles();
        if( contents != null )
        {
            for( int i = 0; i < contents.length; i++ )
            {
                File content = contents[i];
                if( content.isDirectory() )
                {
                    String targetRawUri;
                    if( isDestinationSAFDirectory )
                        targetRawUri = FindSAFEntryWithNameOrCreateNew( context, destinationRawUri, destinationContents, true, content.getName() );
                    else
                        targetRawUri = new File( destinationDirectory, content.getName() ).getPath();

                    CopyRawDirectoryRecursively( context, content, targetRawUri, isDestinationSAFDirectory );
                }
                else
                {
                    if( isDestinationSAFDirectory )
                    {
                        String targetRawUri = FindSAFEntryWithNameOrCreateNew( context, destinationRawUri, destinationContents, false, content.getName() );
                        WriteToSAFEntry( context, targetRawUri, content.getPath(), false );
                    }
                    else
                    {
                        String targetRawUri = new File( destinationDirectory, content.getName() ).getPath();
                        CopyRawFile( content.getPath(), targetRawUri );
                    }
                }
            }
        }
    }

    @TargetApi( Build.VERSION_CODES.Q )
    private static void DeleteRawDirectoryRecursively( File directory )
    {
        File[] contents = directory.listFiles();
        if( contents != null )
        {
            for( int i = 0; i < contents.length; i++ )
            {
                if( contents[i].isDirectory() )
                    DeleteRawDirectoryRecursively( contents[i] );
                else
                    contents[i].delete();
            }
        }

        directory.delete();
    }

    @TargetApi( Build.VERSION_CODES.Q )
    private static String FindSAFEntryWithNameOrCreateNew( Context context, String folderRawUri, ArrayList<SAFEntry> folderContents, boolean isDirectory, String entryName )
    {
        for( int i = 0; i < folderContents.size(); i++ )
        {
            SAFEntry entry = folderContents.get( i );
            if( entry.getName().equals( entryName ) )
            {
                if( entry.isDirectory() == isDirectory )
                    return entry.getUri().toString();
                else
                {
                    // SAF entry's type doesn't match the type we want, delete the entry
                    entry.delete();
                    break;
                }
            }
        }

        return CreateSAFEntry( context, folderRawUri, isDirectory, entryName );
    }

    static String GetExtensionFromPath(String path){
        int index = path.lastIndexOf(".");
        String ext = path.substring(index);
        return ext;
    }


    ////////////////////////////////////
    ////        MEDIA STORE API     ////
    ////////////////////////////////////

    public static String SaveMedia(Context context, int mediaType, String originalFilePath, String filename, String directoryPath , String extension, String mimeType)
    {
        File originalFile = new File( originalFilePath );
        if( !originalFile.exists() )
        {
            Log.e( "Unity", "Original media file is missing or inaccessible!" );
            return "";
        }

        //int pathSeparator = originalFilePath.lastIndexOf( '/' );
        //int extensionSeparator = originalFilePath.lastIndexOf( '.' );
        //String filename = pathSeparator >= 0 ? originalFilePath.substring( pathSeparator + 1 ) : originalFilePath;
        //String extension = extensionSeparator >= 0 ? originalFilePath.substring( extensionSeparator + 1 ) : "";

        // Credit: https://stackoverflow.com/a/31691791/2373034
        //String mimeType = extension.length() > 0 ? MimeTypeMap.getSingleton().getMimeTypeFromExtension( extension.toLowerCase( Locale.ENGLISH ) ) : null;

        ContentValues values = new ContentValues();
        values.put( MediaStore.MediaColumns.TITLE, filename );
        values.put( MediaStore.MediaColumns.DISPLAY_NAME, filename );
        values.put( MediaStore.MediaColumns.DATE_ADDED, System.currentTimeMillis() / 1000 );

        if( mimeType != null && mimeType.length() > 0 )
            values.put( MediaStore.MediaColumns.MIME_TYPE, mimeType );

        Uri externalContentUri;
        if( mediaType == 0 ) {
            externalContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        }
        else if( mediaType == 1 ){
            externalContentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        }
        else if( mediaType == 2 ){
            externalContentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        }else {
            externalContentUri = MediaStore.Files.getContentUri("external");
        }

        // Android 10 restricts our access to the raw filesystem, use MediaStore to save media in that case
        if( android.os.Build.VERSION.SDK_INT >= 29 )
        {
            values.put( MediaStore.MediaColumns.RELATIVE_PATH, directoryPath );
            values.put( MediaStore.MediaColumns.DATE_TAKEN, System.currentTimeMillis() );
            values.put( MediaStore.MediaColumns.IS_PENDING, true );

            Uri uri = context.getContentResolver().insert( externalContentUri, values );
            if( uri != null )
            {
                try
                {
                    if( WriteFileToStream( originalFile, context.getContentResolver().openOutputStream( uri ) ) )
                    {
                        values.put( MediaStore.MediaColumns.IS_PENDING, false );
                        context.getContentResolver().update( uri, values, null, null );
                    }

                    DocumentFile file = DocumentFileCompat.fromUri(context, uri);
                    return DocumentFileUtils.getAbsolutePath(file);
                }
                catch( Exception e )
                {
                    Log.e( "Unity", "Exception:", e );
                    context.getContentResolver().delete( uri, null, null );
                }
            }

            return "";
        }
        else
        {
            File directory = new File(Environment.getExternalStorageDirectory() + "/" +directoryPath);
            directory.mkdirs();

            File file;
            int fileIndex = 1;
            String filenameWithoutExtension = extension.length() > 0 ? filename.substring( 0, filename.length() - extension.length() - 1 ) : filename;
            String newFilename = filename;
            do
            {
                file = new File( directory, newFilename );
                newFilename = filenameWithoutExtension + fileIndex++;
                if( extension.length() > 0 )
                    newFilename += "." + extension;
            } while( file.exists() );

            try
            {
                if( WriteFileToStream( originalFile, new FileOutputStream( file ) ) )
                {
                    values.put( MediaStore.MediaColumns.DATA, file.getAbsolutePath() );
                    context.getContentResolver().insert( externalContentUri, values );

                    Log.d( "Unity", "Saved media to: " + file.getPath() );

                    // Refresh the Gallery
                    Intent mediaScanIntent = new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE );
                    mediaScanIntent.setData( Uri.fromFile( file ) );
                    context.sendBroadcast( mediaScanIntent );

                    return file.getAbsolutePath();
                }
            }
            catch( Exception e )
            {
                Log.e( "Unity", "Exception:", e );
            }

            return  "";
        }
    }


    private static boolean WriteFileToStream( File file, OutputStream out )
    {
        try
        {
            InputStream in = new FileInputStream( file );
            try
            {
                byte[] buf = new byte[1024];
                int len;
                while( ( len = in.read( buf ) ) > 0 )
                    out.write( buf, 0, len );
            }
            finally
            {
                try
                {
                    in.close();
                }
                catch( Exception e )
                {
                    Log.e( "Unity", "Exception:", e );
                }
            }
        }
        catch( Exception e )
        {
            Log.e( "Unity", "Exception:", e );
            return false;
        }
        finally
        {
            try
            {
                out.close();
            }
            catch( Exception e )
            {
                Log.e( "Unity", "Exception:", e );
            }
        }

        return true;
    }
}
