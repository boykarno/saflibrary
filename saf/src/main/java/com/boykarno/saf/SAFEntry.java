package com.boykarno.saf;


import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import androidx.documentfile.provider.DocumentFile;

import com.anggrayudi.storage.file.DocumentFileCompat;
import com.anggrayudi.storage.file.DocumentFileUtils;

import java.util.ArrayList;


// A Storage Access Framework (SAF) file/folder
@TargetApi( Build.VERSION_CODES.Q )
public class SAFEntry
{
    private static final String TAG = "DocumentFile";

    private Context mContext;
    private Uri mUri;

    public static SAFEntry fromTreeUri( Context context, Uri uri )
    {
        uri = DocumentsContract.buildDocumentUriUsingTree( uri, DocumentsContract.getTreeDocumentId( uri ) );
        if( uri == null )
            return null;

        return new SAFEntry( context, uri );
    }

    public SAFEntry( Context context, Uri uri )
    {
        mContext = context;
        mUri = uri;
    }

    public SAFEntry createFile( String mimeType, String displayName )
    {
        try
        {
            final Uri result = DocumentsContract.createDocument( mContext.getContentResolver(), mUri, mimeType, displayName );
            return ( result != null ) ? new SAFEntry( mContext, result ) : null;
        }
        catch( Exception e )
        {
            Log.e( "Unity", "Exception:", e );
            return null;
        }
    }

    public SAFEntry createDirectory( String displayName )
    {
        try
        {
            final Uri result = DocumentsContract.createDocument( mContext.getContentResolver(), mUri, DocumentsContract.Document.MIME_TYPE_DIR, displayName );
            return ( result != null ) ? new SAFEntry( mContext, result ) : null;
        }
        catch( Exception e )
        {
            Log.e( "Unity", "Exception:", e );
            return null;
        }
    }

    public Uri getUri()
    {
        return mUri;
    }

    /*
    public String getRealPath() {
        DocumentFile file = DocumentFileCompat.fromUri(mContext, getUri());
        String path = DocumentFileUtils.getAbsolutePath(file);
        return path;
    }
*/

    public String getAbsolutePath(){
        String result = "";
        DocumentFile file = DocumentFileCompat.fromUri(mContext, getUri());
        result = DocumentFileUtils.getAbsolutePath(file);

        if(result != "")
            return result;

        Cursor cursor = mContext.getContentResolver().query(mUri, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = mUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
            if(idx < 0)
                cursor.getColumnIndex(MediaStore.Downloads.DATA);
            if(idx < 0)
                cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
            if(idx < 0)
                cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            if(idx < 0)
                cursor.getColumnIndex(MediaStore.DownloadColumns.DATA);

            if(idx >=0)
                result = cursor.getString(idx);

            cursor.close();
        }

        if(result == "")
            result = FileUtils.getPath(mContext, mUri);

        return result;
    }

    public String getName()
    {
        return queryForString( DocumentsContract.Document.COLUMN_DISPLAY_NAME, null );
    }

    public String getType()
    {
        final String rawType = getRawType();
        if( DocumentsContract.Document.MIME_TYPE_DIR.equals( rawType ) )
        {
            return null;
        }
        else
        {
            return rawType;
        }
    }

    public boolean isDirectory()
    {
        return DocumentsContract.Document.MIME_TYPE_DIR.equals( getRawType() );
    }

    public boolean isFile()
    {
        final String type = getRawType();
        if( DocumentsContract.Document.MIME_TYPE_DIR.equals( type ) || TextUtils.isEmpty( type ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public long lastModified()
    {
        return queryForLong( DocumentsContract.Document.COLUMN_LAST_MODIFIED, 0 );
    }

    public long length()
    {
        return queryForLong( DocumentsContract.Document.COLUMN_SIZE, 0 );
    }

    public boolean canRead()
    {
        // Ignore if grant doesn't allow read
        if( mContext.checkCallingOrSelfUriPermission( mUri, Intent.FLAG_GRANT_READ_URI_PERMISSION )
                != PackageManager.PERMISSION_GRANTED )
        {
            return false;
        }
        // Ignore documents without MIME
        if( TextUtils.isEmpty( getRawType() ) )
        {
            return false;
        }
        return true;
    }

    public boolean canWrite()
    {
        // Ignore if grant doesn't allow write
        if( mContext.checkCallingOrSelfUriPermission( mUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION )
                != PackageManager.PERMISSION_GRANTED )
        {
            return false;
        }
        final String type = getRawType();
        final int flags = queryForInt( DocumentsContract.Document.COLUMN_FLAGS, 0 );
        // Ignore documents without MIME
        if( TextUtils.isEmpty( type ) )
        {
            return false;
        }
        // Deletable documents considered writable
        if( ( flags & DocumentsContract.Document.FLAG_SUPPORTS_DELETE ) != 0 )
        {
            return true;
        }
        if( DocumentsContract.Document.MIME_TYPE_DIR.equals( type )
                && ( flags & DocumentsContract.Document.FLAG_DIR_SUPPORTS_CREATE ) != 0 )
        {
            // Directories that allow create considered writable
            return true;
        }
        else if( !TextUtils.isEmpty( type )
                && ( flags & DocumentsContract.Document.FLAG_SUPPORTS_WRITE ) != 0 )
        {
            // Writable normal files considered writable
            return true;
        }
        return false;
    }

    public boolean delete()
    {
        try
        {
            return DocumentsContract.deleteDocument( mContext.getContentResolver(), mUri );
        }
        catch( Exception e )
        {
            Log.e( "Unity", "Exception:", e );
        }

        return false;
    }

    public boolean exists()
    {
        final ContentResolver resolver = mContext.getContentResolver();
        Cursor c = null;
        try
        {
            c = resolver.query( mUri, new String[] {
                    DocumentsContract.Document.COLUMN_DOCUMENT_ID }, null, null, null );
            return c.getCount() > 0;
        }
        catch( Exception e )
        {
            Log.w( TAG, "Failed query: " + e );
            return false;
        }
        finally
        {
            try
            {
                if( c != null )
                    c.close();
            }
            catch( Exception e )
            {
                Log.e( TAG, "Exception:", e );
            }
        }
    }

    public ArrayList<SAFEntry> listFiles()
    {
        final ContentResolver resolver = mContext.getContentResolver();
        final Uri childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree( mUri,
                DocumentsContract.getDocumentId( mUri ) );
        final ArrayList<SAFEntry> results = new ArrayList<SAFEntry>();
        Cursor c = null;
        try
        {
            c = resolver.query( childrenUri, new String[] {
                    DocumentsContract.Document.COLUMN_DOCUMENT_ID }, null, null, null );
            while( c.moveToNext() )
            {
                final String documentId = c.getString( 0 );
                final Uri documentUri = DocumentsContract.buildDocumentUriUsingTree( mUri,
                        documentId );
                results.add( new SAFEntry( mContext, documentUri ) );
            }
        }
        catch( Exception e )
        {
            Log.w( "Unity", "Failed query: " + e );
        }
        finally
        {
            try
            {
                if( c != null )
                    c.close();
            }
            catch( Exception e )
            {
                Log.e( TAG, "Exception:", e );
            }
        }

        return results;
    }

    public String renameTo( String displayName )
    {
        try
        {
            final Uri result = DocumentsContract.renameDocument( mContext.getContentResolver(), mUri, displayName );
            if( result != null )
                mUri = result;
        }
        catch( Exception e )
        {
            Log.e( "Unity", "Exception:", e );
        }

        return mUri.toString();
    }

    private String getRawType()
    {
        return queryForString( DocumentsContract.Document.COLUMN_MIME_TYPE, null );
    }

    private String queryForString( String column, String defaultValue )
    {
        final ContentResolver resolver = mContext.getContentResolver();
        Cursor c = null;
        try
        {
            c = resolver.query( mUri, new String[] { column }, null, null, null );
            if( c.moveToFirst() && !c.isNull( 0 ) )
                return c.getString( 0 );

            return defaultValue;
        }
        catch( Exception e )
        {
            Log.w( TAG, "Failed query: " + e );
            return defaultValue;
        }
        finally
        {
            try
            {
                if( c != null )
                    c.close();
            }
            catch( Exception e )
            {
                Log.e( TAG, "Exception:", e );
            }
        }
    }

    private int queryForInt( String column, int defaultValue )
    {
        return (int) queryForLong( column, defaultValue );
    }

    private long queryForLong( String column, long defaultValue )
    {
        final ContentResolver resolver = mContext.getContentResolver();
        Cursor c = null;
        try
        {
            c = resolver.query( mUri, new String[] { column }, null, null, null );
            if( c.moveToFirst() && !c.isNull( 0 ) )
                return c.getLong( 0 );

            return defaultValue;
        }
        catch( Exception e )
        {
            Log.w( TAG, "Failed query: " + e );
            return defaultValue;
        }
        finally
        {
            try
            {
                if( c != null )
                    c.close();
            }
            catch( Exception e )
            {
                Log.e( TAG, "Exception:", e );
            }
        }
    }

    public String ToRawData(){
        return getName() + "#" + getUri() + "#" + getAbsolutePath() + "#" + (isDirectory()? "d" : "f" );
    }
}
