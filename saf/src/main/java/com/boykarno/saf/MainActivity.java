/// THIS IS JUST A TEST CLASS

package com.boykarno.saf;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    public static MainActivity instance;

    static final int REQUEST_CODE_STORAGE_ACCESS = 8;

    static DirectoryReceiver directoryReceiver;
    static String initialPath;
    static int pickMode;
    static Object filtersObj;

    static Mode mode = Mode.None;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;

        if(mode == Mode.FilePicker){
            SAF.PickSAFEntry(instance, directoryReceiver, initialPath, pickMode, filtersObj);
        }
    }

    public static void Init(Activity activity) {
        // Creating an intent with the current activity and the activity we wish to start
        Intent myIntent = new Intent(activity, MainActivity.class);
        activity.startActivity(myIntent);
    }

    public static void OpenFilePicker(Activity activity, DirectoryReceiver _directoryReceiver, String _initialPath , int _pickMode, Object _filtersObj){
        mode = Mode.FilePicker;

        directoryReceiver = _directoryReceiver;
        initialPath = _initialPath;
        pickMode = _pickMode;
        filtersObj = _filtersObj;
        Init(activity);
    }

    /*
    public static void OpenFolderPicker(){
        if (instance == null)
            return;

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        instance.startActivityForResult(intent, REQUEST_CODE_STORAGE_ACCESS);
    }
*/

    enum Mode {
        None,
        FilePicker
    }
}
